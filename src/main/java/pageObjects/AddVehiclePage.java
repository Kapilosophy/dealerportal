package pageObjects;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddVehiclePage {

	WebDriver driver;
	public WebDriverWait webdwait;

	private By vehicleUse = By.id("VehicleUse");

	public AddVehiclePage(WebDriver driver) {

		this.driver = driver;

	}

	public void VehicleUse() {
		click(vehicleUse);
	}

	private void click(By locator) {

		webdwait = new WebDriverWait(driver, Duration.ofSeconds(10));

		WebElement element = driver.findElement(locator);
		webdwait.until(ExpectedConditions.presenceOfElementLocated(locator)).click();

		element.click();
	}

}
