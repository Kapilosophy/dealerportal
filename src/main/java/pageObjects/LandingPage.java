package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage {

	WebDriver driver;

	public LandingPage(WebDriver driver) {

		this.driver = driver;

	}

	private By Username = By.id("username");
	private By Password = By.id("password");
	private By LoginBTN = By.xpath("//span[text()='Log in']");
	private By selectDealership = By.id(":r8:");
	private By nextBtn = By.xpath("//span[text()='Next']");

	public void LoginSendKeys(String username, String password) {

		driver.findElement(Username).sendKeys(username);
		driver.findElement(Password).sendKeys(password);

	}

	public void DologinClick() {
		driver.findElement(LoginBTN).click();
	}

	public void clickOnDealership() {
		click(selectDealership);

	}

	public void SendKeysDealship(String delaershipName) {

		driver.findElement(selectDealership).sendKeys(delaershipName);

	}

	public void clickOnNextButton() {
		click(nextBtn);
	}

	private void click(By locator) {
		WebElement element = driver.findElement(locator);
		element.click();
	}

}
