Feature: Testing the Quote functionality from dealer quote page

  @test
  Scenario Outline: 1 Testing the login functionality
    Given User is on Dealer quote landing page
    And User login into applcation with <username> and <password>
    Then User is navigated to the home page
    Then User searched the registration number in the new Quote <Registration number>
    Then User filled the add vehicle page fields
    Then User clicks on PMVs quick quote and fills <DriverAge> <Gender> <Vehiclekeptpostcode>
    And User clicks vehicle kept address lookup and fills <vehiclekeptaddress>
    Then User clicks on Next
    And User clicks on QuickQuote for PMV

    Examples: 
      | username   | password    | Registration number | DriverAge | Gender | Vehiclekeptpostcode | vehiclekeptaddress | OdometerValue|
      | D_CWaghare | CHaru@@1120 | Audi                |        27 | Male   |                 111 | New Zealand        | 100| 
