package stepDefinitions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pageObjects.LandingPage;
import utils.Excelutility;
import utils.TestContextSetup;

public class LandingPageStepDefinition {
	public WebDriver driver;
	public WebDriverWait webdwait;
	TestContextSetup testcontextsetup;
	LandingPage landingpage;
	Excelutility exceldata = new Excelutility();

	public LandingPageStepDefinition(TestContextSetup testcontextsetup) {

		this.testcontextsetup = testcontextsetup;
	}

	@Given("User is on Dealer quote landing page")
	public void user_is_on_dealer_quote_landing_page() {

		testcontextsetup.driver = new ChromeDriver();
		testcontextsetup.driver.navigate().to("https://staging-portal.providentsandbox.co.nz/login");
		testcontextsetup.driver.manage().window().maximize();
		testcontextsetup.driver.manage().deleteAllCookies();

	}

	@And("^User login into applcation with (.+) and (.+)$")
	public void user_login_into_applcation_with_and(String username, String password) {

		webdwait = new WebDriverWait(testcontextsetup.driver, Duration.ofSeconds(10));
		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.id("username"))).click();

		landingpage = new LandingPage(testcontextsetup.driver);
		landingpage.LoginSendKeys(username, password);
		landingpage.DologinClick();

	}

	@Then("User is navigated to the home page")
	public void user_is_navigated_to_the_home_page() throws EncryptedDocumentException, IOException {

		String excelFile = "src/test/resource/TestData/Book1.xlsx";
		String Agency = "Sheet1";
		int columnIndex = 1;
		FileInputStream fis = new FileInputStream(new File(excelFile));
		Workbook workbook = WorkbookFactory.create(fis);
		Sheet sheet = workbook.getSheet(Agency);

		Iterator<Map.Entry<String, Map<String, String>>> iterator = exceldata.getAllDataInMap(excelFile, Agency)
				.entrySet().iterator();
		int lastRow = sheet.getLastRowNum();
		int rowCount = lastRow + 1;
		System.out.println(rowCount + " is data row count");
		iterator.hasNext();
		Map.Entry<String, Map<String, String>> agent = iterator.next();
		String Dealershipname = agent.getValue().get("DealerShip");
		System.out.println("Dealership name is: " + Dealershipname);

		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.id(":r8:"))).click();
		landingpage.SendKeysDealship(Dealershipname);
		WebElement inputField = testcontextsetup.driver.findElement(By.id(":r8:"));

		inputField.sendKeys(Keys.ARROW_DOWN);
		inputField.sendKeys(Keys.ENTER);
		landingpage.clickOnNextButton();

	}

	@Then("^User searched the registration number in the new Quote (.+)$")
	public void user_searched_the_registration_number(String RegistrationNumber) throws IOException {

		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='New Quote']"))).click();
//		testcontextsetup.driver.findElement(By.id("registration")).click();
		testcontextsetup.driver.findElement(By.id("registration")).sendKeys(RegistrationNumber);
		testcontextsetup.driver.findElement(By.xpath("(//span[text()='Search' ] )[1]")).click();

//		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.id("registration-label"))).click();
//				.sendKeys(RegistrationNumber);

	}

}
