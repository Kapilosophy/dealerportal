package stepDefinitions;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.AddVehiclePage;
import utils.TestContextSetup;

public class AddVehicleStepDefinition {

	public WebDriver driver;
	TestContextSetup testcontextsetup;
	public WebDriverWait webdwait;

	AddVehiclePage addvehiclepage;

	public AddVehicleStepDefinition(TestContextSetup testcontextsetup) {
		this.testcontextsetup = testcontextsetup;

	}

	@Then("User filled the add vehicle page fields")
	public void user_filled_the_add_vehicle_page_fields() {

		addvehiclepage = new AddVehiclePage(testcontextsetup.driver);
		webdwait = new WebDriverWait(testcontextsetup.driver, Duration.ofSeconds(10));
		
		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.id("VehicleUse"))).click();
		
//		addvehiclepage.clickOnVehicleUse();

	}

	@When("User selects the vehicle use")
	public void user_selects_the_vehicle_use() {

		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li[@data-value='PRIVATE']"))).click();

//		addvehiclepage.DoClickVehicleUse();

	}

	@And("^Value of Odometer in Kilometer(.+)$")
	public void odometer_in_kilometer(String OdometerValue) {
//		addvehiclepage = new AddVehiclePage(driver);

		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.id("odometer"))).sendKeys(OdometerValue);

//		addvehiclepage.sendKeysOdometer(OdometerValue);

	}

	@Then("User clicks on save")
	public void user_clicks_on_save() {

//		addvehiclepage.doClickSave();

		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Save']"))).click();

	}

	public void userClicksOnQuickQuoteForPMV() {

		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//span[text()='Quick quote'])[1]")))
				.click();

	}

	@Then("^User clicks on PMVs quick quote and fills (.+) (.+) (.+)$")
	public void userClicksOnPMVsQuickQuoteAndFills(String driverAge, String gender, String vehicleKeptPostcode) {
		System.out.println("User clicks on PMVs quick quote");
		System.out.println("Filling driver age: " + driverAge);
		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.id("Age"))).sendKeys(driverAge);

		System.out.println("Filling gender: " + gender);
		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.id("PrincipalDriver_Gender"))).click();
		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//li[@role='option'])[2]"))).click();

		System.out.println("Filling vehicle kept postcode: " + vehicleKeptPostcode);
		webdwait.until(ExpectedConditions.presenceOfElementLocated(By.id("VehicleKeptAddressPC")))
				.sendKeys(vehicleKeptPostcode);

	}

	@Then("^User clicks vehicle kept address lookup and fills (.+)$")
	public void userClicksVehicleKeptAddressLookupAndFills(String vehicleKeptAddress) {
		System.out.println("User clicks vehicle kept address lookup");
		System.out.println("Filling vehicle kept address: " + vehicleKeptAddress);
	}

	@Then("User clicks on Next")
	public void userClicksOnNext() throws InterruptedException {

		Thread.sleep(8000);
		webdwait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Next']"))).click();

//		testcontextsetup.driver.quit();

	}

	@And("User clicks on QuickQuote for PMV")
	public void userClicksOnQuickQuoteForPMV1() {

	}

}
